from django_filters import rest_framework as filters
from rest_framework.filters import SearchFilter

class DjangoFilterBackendStandard(filters.DjangoFilterBackend):
    
    def get_filterset_kwargs(self, request, queryset, view):
        return {
            'data': request.GET,
            'queryset': queryset,
            'request': request,
        }


class StandardSearchFilter(SearchFilter):

    def get_search_terms(self, request):
        """
        Search terms are set by a ?search=... query parameter,
        and may be comma and/or whitespace delimited.
        """
        params = request.GET.get(self.search_param, '')
        params = params.replace('\x00', '')  # strip null characters
        params = params.replace(',', ' ')
        return params.split()
