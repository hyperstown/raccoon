from django.db.models.base import ModelBase


def get_new_fields(model, exclude=['id']):
    """ Get list of fields excluding ones inherited """

    if not isinstance(exclude, list):
        raise ValueError('exclude must be a list. Got %s' % type(exclude))
    
    if not isinstance(model, ModelBase):
        raise ValueError('model must be a ModelBase. Got %s' % type(model))

    all_fields = [field.name for field in model._meta.fields]
    try:
        parent_fields = [field.name for field in model.__bases__[0]._meta.fields]
    except IndexError:
        return all_fields

    return list(set(all_fields) - set(parent_fields) - set(exclude))
