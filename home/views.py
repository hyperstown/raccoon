from django.template import loader
from django.http import HttpResponse

from shows.models import Show

def index(request):
    template = loader.get_template('home/index.html')
    top10 = Show.objects.order_by('-average_score')[:10]
    recently_added =  Show.objects.order_by('-created')[:10]
    context = {
        "top10": top10,
        "recently_added": recently_added
    }
    return HttpResponse(template.render(context, request))
