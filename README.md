# Raccoon

## Installation

```bash
$ python -m venv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
```

## Starting development server



```bash
$ python manage.py runserver 
```

If you starting development server for a first time make sure to run these commands to apply migrations and create new superuser.

```bash
$ python manage.py migrate 
```
```bash
$ python manage.py createsuperuser 
```

[Source](https://github.com/cckuqui/anime-db)