from django.db import models
from django.contrib.auth.models import AbstractUser

# token auth imports
# from django.conf import settings
# from django.db.models.signals import post_save
# from django.dispatch import receiver
# from rest_framework.authtoken.models import Token

class User(AbstractUser):
    """ Custom user class """    
    bio = models.CharField(max_length=250, null=True, blank=True)
    # friends = models.ManyToManyField('self', blank=True)

    def __str__(self) -> str:
        return self.username



# Token auth - Generating Tokens on user.create
# https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication
# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def create_auth_token(sender, instance=None, created=False, **kwargs):
#     if created:
#         Token.objects.create(user=instance)
