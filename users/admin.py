from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from raccoon.utils import get_new_fields
from .models import User

class CustomUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
        (_('Other'), {'fields': (get_new_fields(User))}),
    )

admin.site.register(User, CustomUserAdmin)