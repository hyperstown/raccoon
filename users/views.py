from django.template import loader
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from shows.models import Score

# class someView(LoginRequiredMixin, CreateView):
#     login_url = "/login/"

@login_required(login_url='/login/')
def index(request):
    template = loader.get_template('users/index.html')
    user_shows = Score.objects.filter(user__id=request.user.id)
    context = {
        "user_shows": user_shows,
    }
    return HttpResponse(template.render(context, request))