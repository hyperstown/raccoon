from django.urls import path
from rest_framework import routers
from .views import UsersAPIViewset


router = routers.DefaultRouter()
router.register(r"", UsersAPIViewset, basename="users")

urlpatterns = []

urlpatterns += router.urls