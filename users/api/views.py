from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from ..models import User
from .serializers import UserSerializer, MyAccountSerializer


class UsersAPIViewset(viewsets.ReadOnlyModelViewSet):
    """ Messages ListApiView. Get all messages. """

    permission_classes = [IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    search_fields = ['username']

    @action(detail=False, methods=['get'], url_path='my-account')
    def my_account(self, request, *args, **kwargs):
        user = User.objects.get(id=request.user.id)
        serializer = MyAccountSerializer(user)
        return Response(
            serializer.data
        )

