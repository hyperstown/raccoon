# utils

def char_choices(*choices):
    if len(choices) == 1:
        if not isinstance(choices[0], list):
            raise ValueError("Choices must be a list")
        choices = choices[0]
    
    choices_tuple = tuple()
    for choice in choices:
        choices_tuple += ((choice, choice),)

    return choices_tuple
