function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

const scoreSelects = document.getElementsByClassName('score-select');

for (let i = 0; i < scoreSelects.length; i++) {
    const scoreSpinner = scoreSelects[i].closest('.score-select-wrapper').querySelector('.score-spinner');
    scoreSelects[i].removeAttribute('disabled');
    scoreSelects[i].addEventListener("change", (e) => {
        scoreSpinner.style.visibility = 'visible';
        scoreSelects[i].setAttribute("disabled", "");
        const scoreValue = parseInt(e.target.value);
        const csrftoken = getCookie('csrftoken');
        let showSlug = scoreSelects[i].id
        if(showSlug) showSlug += "/";
        const request = new Request(
            '/api' + window.location.pathname + showSlug + 'my-score/',
            {
                method: 'PUT',
                headers: {
                    'X-CSRFToken': csrftoken,
                    "Content-Type": "application/json",
                },
                mode: 'same-origin', // Do not send CSRF token to another domain.
                body: JSON.stringify({value:scoreValue})
            }
        );
        fetch(request).then(function(response) {
            if(!response.ok){
                console.error(response);
            }
            scoreSpinner.style.visibility = 'hidden';
            scoreSelects[i].removeAttribute('disabled');
            let inlineScore = document.getElementById(showSlug.replace('/', '-score'));
            
            if(inlineScore){
                inlineScore.textContent = JSON.stringify(scoreValue);
            }
        });
    })
}


document.querySelector("#comment-btn")?.addEventListener('click', (e) => {
    //TODO
})