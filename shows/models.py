from django.db import models
from django.contrib.auth import get_user_model
from django.template.defaultfilters import slugify

from .utils import char_choices
from .managers import ShowManager

User = get_user_model()

class Genre(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Studio(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Cast(models.Model):

    full_name = models.CharField(max_length=250)
    role = models.CharField(max_length=100)
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return "%s - %s" % (self.full_name, self.role)

    class Meta:
        verbose_name_plural = "Cast"


class Character(models.Model):

    full_name = models.CharField(max_length=250)
    role = models.CharField(max_length=100)
    image_character = models.ImageField(null=True, blank=True)

    actor = models.ForeignKey(Cast, on_delete=models.DO_NOTHING)

    def __str__(self):
        return "%s - %s" % (self.full_name, self.role)

class Show(models.Model):

    TV_TYPE_CHOICES = char_choices(
        'TV', 'Movie', 'OVA', 'Special', 'ONA'
    )

    STATUS_CHOICES = char_choices(
        'Airing', 'Finished', 'Not aired yet'
    )

    RATING_CHOICES = char_choices(
        'G', 'PG-13', 'R', 'R+', 'Rx'
    )

    title = models.CharField(max_length=250, unique=True)
    slug = models.SlugField(null=False, unique=True, blank=True)
    synopsis = models.TextField()
    tv_type = models.CharField(max_length=8, choices=TV_TYPE_CHOICES, default='TV')
    episodes_no = models.PositiveIntegerField()
    status = models.CharField(max_length=13, choices=STATUS_CHOICES, default='Not aired yet')
    started_airing = models.DateField(null=True, blank=True)
    finished_airing = models.DateField(null=True, blank=True)
    cover = models.ImageField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    rating = models.CharField(max_length=5, choices=RATING_CHOICES, null=True, blank=True)
    genres = models.ManyToManyField(Genre)
    cast = models.ManyToManyField(Cast, blank=True)
    characters = models.ManyToManyField(Character, blank=True)
    studios = models.ManyToManyField(Studio, blank=True)
    references = models.ManyToManyField('self', blank=True)


    objects = ShowManager()
    # annotated fields:
    # average_score = Avg('scores__value')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
            duplicates = self.__class__.objects.filter(slug=self.slug).count()
            if duplicates:
                self.slug = self.slug + '-' + str(duplicates + 1)
        return super().save(*args, **kwargs)


class Score(models.Model):

    SCORE_CHOICES = [
        (10, '(10) Masterpiece'),
        (9, '(9) Great'),
        (8, '(8) Very Good'),
        (7, '(7) Good'),
        (6, '(6) Fine'),
        (5, '(5) Average'),
        (4, '(4) Bad'),
        (3, '(3) Very Bad'),
        (2, '(2) Horrible'),
        (1, '(1) Appalling'),
    ]

    value = models.PositiveSmallIntegerField(choices=SCORE_CHOICES)
    show = models.ForeignKey(Show, on_delete=models.CASCADE, related_name='scores')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='scores')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s - %s - %s" % (self.show.title, self.value, self.user.username)

    class Meta:
        unique_together = ['show', 'user']


class Comment(models.Model):
    show = models.ForeignKey(
        Show, related_name='comments', on_delete=models.CASCADE
    )
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments')
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    edited = models.BooleanField(default=False)

    def __str__(self):
        return "'%s' by '%s'" % (self.id, self.author)

    class Meta:
        ordering = ('-created_at', '-id')


class CommentVote(models.Model):
    is_up = models.BooleanField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='votes')
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, related_name='votes')
