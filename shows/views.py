from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.http import QueryDict, HttpResponseRedirect
from django.db.models import OuterRef, Subquery, PositiveSmallIntegerField, Count
from django.views.generic import ListView, DetailView, CreateView


from rest_framework.permissions import AllowAny, IsAuthenticated

from raccoon.mixins import PermissionsMixin, FilterMixin
from raccoon.filters import StandardSearchFilter, DjangoFilterBackendStandard

from .models import Show, Score, Comment

User = get_user_model()

class ShowListView(PermissionsMixin, FilterMixin, ListView):

    queryset = Show.objects.order_by('-id')
    paginate_by = 15
    permission_classes = [AllowAny]
    template_name = 'shows/index.html'
    context_object_name = 'shows'
    # filterset_class = ShowFilterSet
    filter_backends = [StandardSearchFilter, DjangoFilterBackendStandard]
    search_fields = ['title']
    extra_context = {'SCORE_CHOICES': Score.SCORE_CHOICES}

    def get(self, request, *args, **kwargs):
        
        self.queryset = self.queryset.annotate(
            user_score=Subquery(
                User.objects.filter(
                    id=request.user.id, scores__show__id=OuterRef('pk')
                ).values('scores__value')[:1], 
                output_field=PositiveSmallIntegerField()
            )
        )
        self.queryset = self.filter_queryset(self.queryset)
        return super().get(request, *args, **kwargs)


class ShowDetailView(PermissionsMixin, DetailView):

    model = Show
    permission_classes = [AllowAny]
    template_name = 'shows/detail.html'
    context_object_name = 'show'
    extra_context = {'SCORE_CHOICES': Score.SCORE_CHOICES}

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.rank = self.model.objects.filter(average_score__gte=self.object.average_score).count()
        self.object.members = Score.objects.filter(show=self.object).count()
        if request.user.id:
            try:
                self.object.user_score = request.user.scores.get(show=self.object.pk).value
            except Score.DoesNotExist: 
                pass   
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class ShowCommentCreateView(PermissionsMixin, CreateView):

    model = Comment
    fields = ['content']
    template_name = 'shows/detail.html'
    permission_classes = [IsAuthenticated]

    def get_success_url(self):
        return reverse('show-detail', kwargs=self.kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('show-detail', kwargs=kwargs))

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.show = get_object_or_404(
            Show, slug=self.kwargs.get('slug', '')
        )
        return super().form_valid(form)

class TopShowListView(PermissionsMixin, ListView):

    queryset = Show.objects.order_by('-average_score')
    paginate_by = 50
    permission_classes = [AllowAny]
    template_name = 'shows/top.html'
    context_object_name = 'shows'
    extra_context = {'SCORE_CHOICES': Score.SCORE_CHOICES}

    def get(self, request, *args, **kwargs):
        
        self.queryset = self.queryset.annotate(
            user_score=Subquery(
                User.objects.filter(
                    id=request.user.id, scores__show__id=OuterRef('pk')
                ).values('scores__value')[:1], 
                output_field=PositiveSmallIntegerField()
            ),
            votes=Count('scores'),
        )
        return super().get(request, *args, **kwargs)