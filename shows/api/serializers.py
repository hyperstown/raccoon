from rest_framework import serializers

from ..models import Show, Score, Comment

class ShowSerializer(serializers.ModelSerializer):

    class Meta:
        model = Show
        fields = '__all__'


class ScoreSerializer(serializers.ModelSerializer):

    class Meta:
        model = Score
        fields = '__all__'
        read_only_fields = ['user']
        
class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = '__all__'
        read_only_fields = ['author', 'edited', 'show']