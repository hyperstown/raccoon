# import time
from django.shortcuts import get_object_or_404
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from ..models import Show, Score, Comment
from ..permission import IsAdminUserOrReadOnly, IsCommentAuthorOrReadOnly
from .serializers import ScoreSerializer, ShowSerializer, CommentSerializer

methods = ['get', 'post', 'put', 'patch', 'delete']

class ShowAPIViewset(viewsets.ModelViewSet):

    permission_classes = [IsAdminUserOrReadOnly]
    queryset = Show.objects.all()
    serializer_class = ShowSerializer
    search_fields = ['title']
    lookup_field = 'slug'
    lookup_url_kwarg = 'slug'

    @action(detail=True, methods=methods, url_path='my-score', permission_classes=[IsAuthenticated])
    def my_score(self, request, *args, **kwargs):
        self.serializer_class = ScoreSerializer
        if request.method.lower() == 'get':
            show = self.get_object()
            score = Score.objects.filter(show=show, user=request.user).first()
            serializer = self.serializer_class(score)
            return Response((serializer.data))
        elif request.method.lower() == 'post':
            data = {
                "user": request.user,
                "show": self.get_object(),
                "value": request.data.get("value")
            }
            serializer = self.serializer_class(data=data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        elif request.method.lower() == 'patch' or request.method.lower() == 'put':
            data = {
                "user": request.user.pk,
                "show": self.get_object().pk,
                "value": request.data.get("value")
            }
            show = self.get_object()
            score = Score.objects.filter(show=show, user=request.user).first()
            serializer = self.get_serializer(score, data=data, partial=bool(request.method.lower() == 'patch'))
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            if getattr(score, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                score._prefetched_objects_cache = {}

            return Response(serializer.data)
        elif request.method.lower() == 'delete':
            show = self.get_object()
            score = Score.objects.filter(show=show, user=request.user).first()
            score.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({"detail": "Method not allowed! %s" % request.method}, status=status.HTTP_405_METHOD_NOT_ALLOWED)


class ScoreAPIViewset(viewsets.ModelViewSet):

    permission_classes = [IsCommentAuthorOrReadOnly]
    queryset = Score.objects.all()
    serializer_class = ScoreSerializer

    def get_queryset(self):
        return super().get_queryset().filter(
            show__slug=self.kwargs['shows_slug']
        )

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)
    

class CommentsAPIViewset(viewsets.ModelViewSet):

    permission_classes = [IsCommentAuthorOrReadOnly]
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def get_queryset(self):
        return super().get_queryset().filter(
            show__slug=self.kwargs['shows_slug']
        )

    def perform_create(self, serializer):
        show = get_object_or_404(Show, slug=self.kwargs['shows_slug'])
        serializer.save(author=self.request.user, show=show)

    def perform_update(self, serializer):
        show = get_object_or_404(Show, slug=self.kwargs['shows_slug'])
        serializer.save(author=self.request.user, edited=True, show=show)
    