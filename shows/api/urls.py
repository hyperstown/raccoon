from django.urls import path
from django.conf.urls import include

from rest_framework_nested import routers
from . import views

router = routers.DefaultRouter()
router.register(r"", views.ShowAPIViewset, basename="shows")

shows_router = routers.NestedSimpleRouter(router, r"", lookup='shows')
shows_router.register(r'scores', views.ScoreAPIViewset, basename='show-scores')
shows_router.register(r'comments', views.CommentsAPIViewset, basename='show-comments')


urlpatterns = [
    path(r"", include(router.urls)),
    path(r"", include(shows_router.urls)),
]