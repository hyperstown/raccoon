# Generated by Django 4.0.4 on 2022-05-28 13:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shows', '0017_alter_show_cast_alter_show_characters'),
    ]

    operations = [
        migrations.CreateModel(
            name='Studio',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='show',
            name='studios',
            field=models.ManyToManyField(blank=True, to='shows.studio'),
        ),
    ]
