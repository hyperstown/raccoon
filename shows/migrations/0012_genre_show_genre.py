# Generated by Django 4.0.4 on 2022-05-27 15:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shows', '0011_rename_added_to_database_show_created_score_created'),
    ]

    operations = [
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('choice', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='show',
            name='genre',
            field=models.ManyToManyField(related_name='genres', to='shows.genre'),
        ),
    ]
