# Generated by Django 4.0.4 on 2022-05-28 13:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shows', '0019_show_rating_show_references'),
    ]

    operations = [
        migrations.AlterField(
            model_name='score',
            name='value',
            field=models.PositiveSmallIntegerField(choices=[(10, '(10) Masterpiece'), (9, '(9) Great'), (8, '(8) Very Good'), (7, '(7) Good'), (6, '(6) Fine'), (5, '(5) Average'), (4, '(4) Bad'), (3, '(3) Very Bad'), (2, '(2) Horrible'), (1, '(1) Appalling')]),
        ),
    ]
