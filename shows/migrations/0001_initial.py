# Generated by Django 4.0.4 on 2022-04-27 19:48

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Show',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=250)),
                ('synopsis', models.TextField()),
                ('tv_type', models.CharField(choices=[('tv', 'tv'), ('movie', 'movie'), ('ova', 'ova'), ('special', 'special'), ('ona', 'ona')], default='tv', max_length=15)),
                ('eposodes_no', models.PositiveIntegerField()),
                ('status', models.CharField(choices=[('tv', 'tv'), ('movie', 'movie'), ('ova', 'ova'), ('special', 'special'), ('ona', 'ona')], default='airing', max_length=15)),
                ('started_airing', models.DateField()),
                ('ended_airing', models.DateField()),
                ('cover', models.ImageField(null=True, upload_to='')),
            ],
        ),
    ]
