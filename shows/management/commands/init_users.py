import json
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

from tqdm import tqdm

User = get_user_model()


class Command(BaseCommand):
    help = 'Initialize example users'

    def handle(self, *args, **options):
        users_list_file = open("shows/management/commands/assets/users.json", "r")
        users_json = json.loads(users_list_file.read())
        users_list_file.close()

        created_no = 0

        for user in tqdm(users_json):
            try:
                _, created = User.objects.get_or_create(username=user['username'], password='')
            except BaseException as e:
                self.stdout.write(self.style.ERROR("Error for user '%s'. Details: %s" % (user['username'], e)))
                break
            if created:
                created_no += 1

        self.stdout.write(self.style.SUCCESS(f'Successfully created {created_no} users'))