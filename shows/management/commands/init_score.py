import json
from random import randint
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

from tqdm import tqdm

User = get_user_model()

from shows.models import Score, Show

class Command(BaseCommand):
    help = 'Initialize example shows entires'

    def handle(self, *args, **options):
       
        anime_list_file = open("shows/management/commands/assets/anime.json", "r")
        anime_json = json.loads(anime_list_file.read())
        anime_list_file.close()

        # ratio
        users_count = User.objects.all().count()
        max_members = 0
        for anime in anime_json:
            if anime.get("members", 0) > max_members:
                max_members = anime.get("members", 0)
        
        ratio = users_count / max_members

        created_no = 0

        for anime in tqdm(anime_json):
            show =  Show.objects.filter(title=anime["anime_title"]).first()
            if show:
                members = anime.get("members", 1000) * ratio
                for user in User.objects.all()[:round(members)]:
                    score_value = anime["score"] + randint(-2, 2)
                    if score_value > 10:
                        score_value = 10
                    elif score_value < 1:
                        score_value = 1
                    _, created = Score.objects.get_or_create(show=show, user=user, defaults={'value': score_value})
                    if created:
                        created_no += 1

        self.stdout.write(self.style.SUCCESS(f'Successfully created {created_no} entries'))