from django.core.management.base import BaseCommand

from tqdm import tqdm

from shows.models import Genre

class Command(BaseCommand):
    help = 'Initialize example shows entires'

    def handle(self, *args, **options):
       
        genres_file = open("shows/management/commands/assets/genres.txt", "r")
        genres = genres_file.read()
        genres = genres.split(',')

        created_no = 0
        # print(genres)

        for genre in tqdm(genres):
            _, created =  Genre.objects.get_or_create(name=genre.capitalize())
            if created:
                created_no += 1

        self.stdout.write(self.style.SUCCESS(f'Successfully created {created_no} entries'))
