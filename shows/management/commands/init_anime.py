import json
import itertools
from os.path import exists
from datetime import datetime
from django.core.files import File
from django.core.management.base import BaseCommand

from shows.models import Genre, Show, Cast, Character, Studio


from tqdm import tqdm

class Command(BaseCommand):
    help = 'Initialize example shows entires'

    def handle(self, *args, **options):
       
        anime_list_file = open("shows/management/commands/assets/anime.json", "r")
        anime_json = json.loads(anime_list_file.read())
        anime_list_file.close()
        created_no = 0

        for anime in tqdm(anime_json):
            title = anime["anime_title"]
            try:
                img_path = anime["anime_image_path"]
                image_id = img_path.split("/anime/")[1].split("/")[1].split(".jpg")[0] # meh but it works
                image_path = f'shows/management/commands/assets/{image_id}.webp'
                if not exists(image_path):
                    raise FileNotFoundError(f"image_id: {image_id}, title: {title}")
            except FileNotFoundError as e:
                image_id = None
                image_path = None
                self.stdout.write(self.style.WARNING(f"Could not extract image. Details: {str(e)}"))

            show_type = anime.get('anime_media_type_string')

            if show_type not in itertools.chain(*Show.TV_TYPE_CHOICES): # type: ignore
                show_type = 'TV'

            obj, created = Show.objects.get_or_create(
                title=anime["anime_title"], defaults={
                    'synopsis' : anime.get("synopsis"),
                    'tv_type' : show_type, 
                    'episodes_no' : anime.get("anime_num_episodes", 1),
                    'status' : 'Finished',
                    'rating': anime["anime_mpaa_rating_string"],
                    'started_airing' : datetime.strptime(anime.get("anime_start_date_string", '1-01-70'), '%d-%m-%y'),
                    'finished_airing' : datetime.strptime(anime.get("anime_end_date_string", '1-01-70'), '%d-%m-%y')
                }
            )

            # genres
            genres_list = anime.get('genres', [])
            for g in genres_list:
                try:
                    genre = Genre.objects.get(name=g["name"])
                    obj.genres.add(genre)
                except Genre.DoesNotExist:
                    pass

            # cast
            cast_list = anime.get('staff', [])
            for ca in cast_list:
                try:
                    cast = Cast.objects.get_or_create(full_name=ca["name"], role=ca["role"])[0]
                    obj.cast.add(cast)
                except Cast.DoesNotExist:
                    pass

            # characters
            characters_list = anime.get('characters', [])
            for cha in characters_list:
                actor = Cast.objects.get_or_create(full_name=cha["actor"], role="VA")[0]
                try:
                    character = Character.objects.get_or_create(full_name=cha["name"], role=cha["role"], actor=actor)[0]
                    obj.characters.add(character)
                except Character.DoesNotExist:
                    pass

            # studios
            studios = anime.get('anime_studios', [])
            for studio in studios:
                try:
                    studio_obj = Studio.objects.get_or_create(name=studio["name"])[0]
                    obj.studios.add(studio_obj)
                except Studio.DoesNotExist:
                    pass

            if created:
                if image_id and image_path:
                    obj.cover.save(f'{image_id}.webp', File(open(image_path, 'rb')), save=True)
                created_no += 1
                # self.stdout.write("%s created!" % anime["anime_title"])

        self.stdout.write(self.style.SUCCESS(f'Successfully created {created_no} entries'))
