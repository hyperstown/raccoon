from django.contrib import admin
from .models import Show, Score, Genre, Cast, Character, Studio

admin.site.register(Show)
admin.site.register(Score)
admin.site.register(Genre)
admin.site.register(Cast)
admin.site.register(Character)
admin.site.register(Studio)
