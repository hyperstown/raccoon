from django.urls import path

from . import views

urlpatterns = [
    path('', views.ShowListView.as_view(), name='show-list'),
    path('top/', views.TopShowListView.as_view(), name='shows-top'),
    path('<slug:slug>/', views.ShowDetailView.as_view(), name='show-detail'),
    path('<slug:slug>/add-comment/', views.ShowCommentCreateView.as_view(), name='add-comment'),
]
