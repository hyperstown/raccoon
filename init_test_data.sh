#!/bin/bash

# -n means string is not null.
# -z means string is null, that is, has zero length

if [ -z ${VIRTUAL_ENV} ]; then 
    echo "No venv detected. Please activate one before running the script";
    exit
fi

echo ${VIRTUAL_ENV} " - ready."

python manage.py migrate
python manage.py createsuperuser
python manage.py init_users
python manage.py init_genres
python manage.py init_anime
python manage.py init_score
